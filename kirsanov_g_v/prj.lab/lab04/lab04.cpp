#include <opencv2/opencv.hpp>
#include <array>
#include <algorithm>
using namespace cv;
using namespace std;

void DrawCircles(Mat& image) {
	Size square(100, 100);
	image = Mat(square.height * 2, square.width * 3, CV_8U, Scalar(0));
	Rect roi(0, 0, square.width, square.height);
	array<int, 6> background_colors = {0, 127, 255, 127, 255, 0};
	array<int, 6> circle_colors = {127, 0, 127, 255, 0, 255};

	for(size_t i = 0; i < circle_colors.size(); ++i) {
		Mat part(square, CV_8U, background_colors[i]);
		Point circle_center = square / 2;
		circle(part, circle_center, 35, circle_colors[i], -1);
		part.copyTo(image(roi));
		roi.x = (roi.x + square.width) % image.size().width;
		roi.y = square.height * ((i + 1) / 3);
	}
}

Mat applyFilter(const Mat& image, const Mat& filter) {
	Mat image_(image.clone());
	image_.convertTo(image_, filter.type());
	filter2D(image_, image_, -1, filter);
	return image_;
}

Mat normalizeToByte(const Mat& image) {
	Mat image_(image.clone());
	double min = 0;
	minMaxLoc(image_, &min, nullptr);

	image_ += min;
	image_ /= (2 * min) / 255;
	image_.convertTo(image_, CV_8U);
	return image_;
}

Mat pow(const Mat& image, const double& power) {
	Mat clone(image.clone());
	pow(image, power, clone);
	return clone;
}


int main() {
	Mat image;
	DrawCircles(image);
	imwrite("lab04.src.png", image);
	Mat sobel = (Mat_<double>(3, 3) << 1, 0, -1, 2, 0, -2, 1, 0, -1);
	Mat first_sobel = applyFilter(image, sobel);
	imwrite("lab04.viz_dx.png", normalizeToByte(first_sobel));
	
	Mat second_sobel = applyFilter(image, sobel.t());
	imwrite("lab04.viz_dy.png", normalizeToByte(second_sobel));

	Mat third_image;
	sqrt(pow(first_sobel, 2) + pow(second_sobel, 2), third_image);
	imwrite("lab04.viz_gradmod.jpg", third_image);
}
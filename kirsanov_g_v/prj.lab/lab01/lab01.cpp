#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <vector>
#include <algorithm>
using namespace cv;

Mat GetRoi(Mat& image, int x, int y, int width, int height){
        const Rect rectangle(x, y, width, height);
        return image(rectangle);
}

std::vector<uint8_t> Gradient(int width, 
                              unsigned int start_color, 
                              unsigned int color_change_step,
                              unsigned int color_change_pixel_step){
        std::vector<uint8_t> gradient(width, 0);
        for (int i = 0; i < width; ++i) {
                bool time_to_change = i % color_change_pixel_step == 0;
                if (i > 0 && time_to_change)
                        start_color += color_change_step;
                gradient[i] = min<int>(start_color, 255);
        }

        return gradient;
}

Mat gamma_correction(unsigned int start_color, 
                     unsigned int color_change_step,
                     unsigned int color_change_pixel_step) {
        Mat image(Mat::zeros(125, 768, CV_8U));
        const int x = 0;
        const int y = 0;
        const int width = 768;
        const int height = 60;
        std::vector<uint8_t> gradient = Gradient(width, 
                                                 start_color, 
                                                 color_change_step, 
                                                 color_change_pixel_step);
        Mat first_roi = GetRoi(image, x, y, width, height);
        for(size_t col = 0; col < first_roi.size().width; ++col)
                first_roi.col(col).setTo(gradient[col]);

        Mat gamma_corrected;
        first_roi.copyTo(gamma_corrected);
        gamma_corrected.convertTo(gamma_corrected, CV_64F);
        gamma_corrected /= 255.;
        pow(gamma_corrected, 2.4, gamma_corrected);
        gamma_corrected *= 255.;
        
        const int second_y = y + height;
        Mat second_roi = GetRoi(image, x, second_y, width, height);
        gamma_corrected.convertTo(second_roi, CV_8U);
        return image;
}

int main() {
        Mat image = Mat::zeros(250, 768, CV_8U);
        gamma_correction(0, 1, 3).copyTo(image(Rect(0, 0, 768, 125)));
        gamma_correction(5, 10, 30).copyTo(image(Rect(0, 125, 768, 125)));
        imwrite("lab01.png", image);
}

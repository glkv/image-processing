#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <vector>
#include <utility>
#include <cmath>
using namespace cv;
using std::string;
using std::vector;

Mat AddAlphaChannel(const Mat& image){
	vector<Mat> channels;
	split(image, channels);

	Mat alpha_channel(image.size(), channels.front().type());
	channels.emplace_back(std::move(alpha_channel));
	Mat result;
	merge(channels, result);
	return result;
}

Mat GetHSV(const Mat& bgra_image){
	Mat bgr_image;
	cvtColor(bgra_image, bgr_image, COLOR_BGRA2BGR);
	Mat hsv_image;
	cvtColor(bgr_image, hsv_image, COLOR_BGR2HSV);
	return hsv_image;
}

Mat GetValueFromHSV(const Mat& hsv_image){
	vector<Mat> hsv_channels;
	split(hsv_image, hsv_channels);
	return hsv_channels.back();
}

Mat GetValueFromBGRA(const Mat& bgra_image){
	Mat hsv = GetHSV(bgra_image);
	return GetValueFromHSV(hsv);
}

Mat Diff(const Mat& source, const Mat& compressed){
	Mat compressed_with_alpha = AddAlphaChannel(compressed);
	vector<Mat> source_channels;
	split(source, source_channels);
	vector<Mat> compressed_channesl;
	split(compressed, compressed_channesl);

	Mat mosaic(source.size().height, source.size().width * 5, source.type());
	Rect roi(0, 0, source.size().width, source.size().height);
	for(size_t channel = 0; channel < 3; ++channel){
		Mat mosaic_part = mosaic(roi);
		vector<Mat> roi_channels;
		split(mosaic_part, roi_channels);

		const Mat& source_channel = source_channels[channel];
		const Mat& compressed_channel = compressed_channesl[channel];
		roi_channels[channel] = source_channel != compressed_channel;
		merge(roi_channels, mosaic_part);
		roi.x += source.size().width;
	}

	Mat value_source = GetValueFromBGRA(source);
	Mat value_difference = GetValueFromBGRA(compressed_with_alpha);
	Mat mask = value_source != value_difference;
	vector<Mat> channels = {mask, mask, mask};
	Mat value;
	merge(channels, value);
	value = AddAlphaChannel(value);
	value.copyTo(mosaic(roi));
	roi.x += source.size().width;
	compressed_with_alpha.copyTo(mosaic(roi));
	return mosaic;
}

string GetJpegPath(int compression_level, string&& prefix){
	string scompression_level = std::to_string(compression_level);
	string path = prefix;
	path += scompression_level + ".jpeg";
	return path;
}

int main(){
	string image_path = "../testdata/compression.png";
	Mat image = imread(image_path, IMREAD_UNCHANGED);

	vector<int> compression_params = {IMWRITE_JPEG_QUALITY, 0};
	Mat result(image.size().height * 2, image.size().width * 5, image.type());
	Rect roi(0, 0, image.size().width * 5, image.size().height);
	for(int compression_level : {95, 65}){
		string path_to_compressed = GetJpegPath(compression_level, "jpeg_");
		compression_params[1] = compression_level;
		imwrite(path_to_compressed, image, compression_params);
		Mat compressed = imread(path_to_compressed);
		Mat difference_mat = Diff(image, compressed);
		difference_mat.copyTo(result(roi));
		roi.y += image.size().height;
	}
	imwrite("lab02.jpeg", result);
}
## Работа 1. Исследование гамма-коррекции
автор: Кирсанов Г.В.
дата: 2020-05-04T20:08:34

<!-- url: https://gitlab.com/glkv/image-processing/-/tree/master/kirsanov_g_v/prj.lab/lab03 -->

### Задание
1. Подобрать и зачитать небольшое изображение $S$ в градациях серого.
2. Построить и нарисовать гистограмму $H_s$ распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график $V$ табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить $L$, нарисовать гистограмму $H_L$ преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения $C_i$ и их гистограммы $H_{C_{i}}$).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное $S$ и бинаризованное $B_G$ изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное $S$ и бинаризованное $B_L$ изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации $M$.
9. Сделать визуализацию $K$ бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).


### Результаты

![](lab03.src.png)
Рис. 1. Исходное полутоновое изображение $S$

![](lab03.hist.src.png)
Рис. 2. Гистограмма $H_s$ исходного полутонового изображения $S$

![](lab03.lut.png)
Рис. 3. Визуализация функции преобразования $V$

![](lab03.lut.src.png)
Рис. 4.1. Таблично пребразованное изображение $L$

![](lab03.hist.lut.src.png)
Рис. 4.2. Гистограмма $H_L$ таблично-пребразованного изображения $L$

![](lab03.clahe.1.png)
Рис. 5.1. Преобразование $C_1$ CLAHE с параметрами ...

![](lab03.hist.clahe.1.png)
Рис. 5.2. Гистограмма $H_{C_{1}}$

![](lab03.clahe.2.png)
Рис. 5.3. Преобразование $C_2$ CLAHE с параметрами ...

![](lab03.hist.clahe.2.png)
Рис. 5.4. Гистограмма $H_{C_{2}}$

![](lab03.clahe.3.png)
Рис. 5.5. Преобразование $C_3$ CLAHE с параметрами ...

![](lab03.hist.clahe.3.png)
Рис. 5.6. Гистограмма $H_{C_{3}}$

![](lab03.bin.global.png)
Рис. 6. Изображение $S$ до и $B_G$ после глобальной бинаризации

![](lab03.bin.local.png)
Рис. 7. Изображение $S$ до и $B_L$ после локальной бинаризации

![](lab03.morph.png)
Рис. 8. До и после морфологической фильтрации $M$

![](lab03.mask.png)
Рис. 9. Визуализация маски $K$

### Текст программы

```cpp
#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <array>
#include <map>
#include <unordered_map>
#include <string>
#include <cmath>
#include <vector>
#include <iostream>
using namespace cv;

template <typename Container>
void CountIntensityOccurancies(Container &container, const Mat &image) {
    Size size = image.size();
    for (size_t i = 0; i < size.height; ++i)
        for (size_t j = 0; j < size.width; ++j)
            ++container[image.at<uint8_t>(i, j)];
}

uint32_t MaxValue(const std::array<uint32_t, 256> &intensity) {
    unsigned int max_value = 1;
    for (const unsigned int &value : intensity)
        if (value > max_value)
            max_value = value;

    return max_value;
}

void DrawHistogram(const std::array<uint32_t, 256> &intensity, 
                   const std::string& image_name) {
    Size size(256, 200);

    int image_color = 255;
    Mat histogram(size, CV_8U, Scalar(image_color));

    uint32_t max_value = MaxValue(intensity);
    Rect main_hist(0, 0, size.width, size.height * 4 / 5);
    Rect gradient(0, main_hist.height + 0.05 * size.height, size.width, 0);
    gradient.height = size.height - gradient.y;
    double relation = main_hist.height * 1.0 / max_value;
    for (size_t i = 0; i < size.width; ++i) {
        int high_point = ceil(intensity.at(i) * relation);
        Rect col(i, 0, 1, main_hist.height - high_point);
        histogram(col).setTo(255 - image_color);
        col.y = main_hist.height;
        col.height = gradient.y - col.y;
        histogram(col).setTo(255 / 2);
        histogram(gradient).col(i).setTo(i);
    }

    imwrite(image_name, histogram);
}

void Histogram(const Mat &image, const std::string& image_name) {
    std::array<uint32_t, 256> counter;
    counter.fill(0);
    CountIntensityOccurancies(counter, image);
    DrawHistogram(counter, image_name);
}

double autocontast(double x,
                   double old_min_value, double old_max_value,
                   double new_min_value, double new_max_value) {
    double multiplier = (new_max_value - new_min_value) /
                        (old_max_value - old_min_value);
    return (x - old_min_value) * multiplier + new_min_value;
}

Mat GetLookUpTable(const Mat &image) {
    double min_value = 0;
    double max_value = 0;
    minMaxLoc(image, &min_value, &max_value);
    Mat lut(1, 256, CV_8U);
    uchar *p = lut.ptr();
    for (int i = 0; i < 256; ++i)
    {
        p[i] = autocontast(i, min_value, max_value, 50, 200);
    }

    return lut;
}

void drawFunc(const Mat &lut) {
    Mat func(256, 256, CV_8U, 255);

    const uchar *p = lut.ptr();
    std::vector<Point> graph;
    int lut_width = lut.size().width;
    graph.reserve(lut_width);
    for (int i = 0; i < lut_width; ++i) {
        graph.emplace_back(i, 255 - p[i]);
    }
    polylines(func, std::vector<std::vector<Point>>{graph}, false, 0, 4);
    imwrite("lab03.lut.png", func);
}

void LUTPart(const Mat& image) {
    Histogram(image, "lab03.hist.src.png");

    Mat lut = GetLookUpTable(image);
    drawFunc(lut);

    Mat result;
    LUT(image, lut, result);
    imwrite("lab03.lut.src.png", result);
    Histogram(result, "lab03.hist.lut.src.png");
}

void ClahePart(const Mat& image) {
    std::array<double, 3> clips = {2.0, 4.0, 8.0};
    std::array<Size, 3> sizes = {Size(8, 8), Size(4, 4), Size(10, 10)};
    for (int i = 1; i <= 3; ++i) {
        Ptr<CLAHE> clahe = createCLAHE(clips[i-1], sizes[i-1]);
        
        Mat clahe_result;
        clahe->apply(image, clahe_result);
        std::string s_i = std::to_string(i); 
        std::string result_name = "lab03.clahe." + s_i + ".png";
        imwrite(result_name, clahe_result);

        std::string hist_name = "lab03.hist.clahe." + s_i + ".png";
        Histogram(clahe_result, hist_name);
    }
}

uint8_t GetHistograMedian(const Mat& image) {
    std::array<uint32_t, 256> counter;
    counter.fill(0);
    CountIntensityOccurancies(counter, image);
    uint32_t total_values = image.size().width * image.size().height;
    uint32_t mid = total_values / 2;
    uint32_t sum = 0;
    for (size_t i = 0; i < counter.size(); ++i) {
	if (sum > mid && i > 0) {
	    return static_cast<uint8_t>(i - 1);
	} else if (sum + counter[i] > mid) {
	    return static_cast<uint8_t>(i);
	}
	sum += counter[i];
    }

    throw std::invalid_argument("Something went wrong");
}

Mat MorphologicalPart(const Mat& image) {
    Size size = image.size();
    Mat result(size.height, size.width * 2, CV_8U, Scalar(0));
    Rect roi(0, 0, size.width, size.height);
    image.copyTo(result(roi));

    Mat kernel = getStructuringElement(MORPH_ELLIPSE, Size(2, 2));
    Mat fixed;
    morphologyEx(image, fixed, MORPH_OPEN, kernel);
    roi.x += roi.width;
    fixed.copyTo(result(roi));
    imwrite("lab03.morph.png", result);
    return fixed;
}

Mat GlobalBinarizationPart(const Mat& image) {
    Mat binarized(image.size(), CV_8U, Scalar(0));
    uint8_t median = GetHistograMedian(image);
    Mat(image.size(), CV_8U, Scalar(0)).copyTo(binarized, image < median);
    Mat(image.size(), CV_8U, Scalar(255)).copyTo(binarized, image >= median);
    imwrite("lab03.bin.global.png", binarized);
    return MorphologicalPart(binarized);
}

void LocalBinarizationPart(const Mat& image) {
    Mat binarized(image.size(), image.type(), Scalar(0));
    adaptiveThreshold(image, binarized, 255, ADAPTIVE_THRESH_GAUSSIAN_C, 
		    THRESH_BINARY, 25, 0);
    imwrite("lab03.bin.local.png", binarized);
}

void MaskPart(const Mat& image, Mat& mask) {
    Mat copy(image.clone());

    cvtColor(copy, copy, COLOR_GRAY2RGB);
    cvtColor(mask, mask, COLOR_GRAY2RGB);
    std::vector<Mat> channels(3);
    split(mask, channels);
    channels[1] = channels[2] = Scalar(0);
    merge(channels, mask);

    Mat result(image.size(), copy.type(), Scalar(0, 0, 0));
    addWeighted(copy, 1, mask, 0.4, 0, result);
    imwrite("lab03.mask.png", result);
}

int main() {
    Mat image = imread("../testdata/gray.png", IMREAD_GRAYSCALE);
    imwrite("lab03.src.png", image);
    LUTPart(image);
    ClahePart(image);
    Mat mask = GlobalBinarizationPart(image);
    MaskPart(image, mask);
    LocalBinarizationPart(image);
}

```
